<?php


namespace App\DataFixtures\Faker\Provider;

use App\Entity\User;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserProvider {
    private UserPasswordEncoderInterface $passwordEncoder;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    public function encodePwd($pwd){
        $user = new User();
        return $passwordEncoded = $this->passwordEncoder->encodePassword($user, $pwd);
    }
}
