Feature: _Offer_
  Background:
    Given the following fixtures files are loaded:
      | user      |
      | offer     |

  Scenario: Get collection
    Given I have the payload
    """
    {
          "email": "admin@gmail.com",
          "password": "Password123"
    }
    """
    Given I authenticate with user "<string>" and password "<string>"
    Given I request "GET /offers"
    Then print last response
    And the response status code should be 200
    And the "hydra:totalItems" property should be an integer equalling "10"
    And reset scope
    Then print last response

  Scenario: Post offer
    Given I have the payload
    """
    {
          "email": "recruiter@gmail.com",
          "password": "Password123"
    }
    """
    Given I authenticate with user "<string>" and password "<string>"
    Given I have the payload
    """
    {
          "name": "Offre1",
          "description": "test description",
          "descriptionCompany": "test description company",
          "typeContract": "CDI",
          "workplace": "Paris",
          "applications": [],
          "recruiter": @user_2,
    }
    """
    Given I request "POST /offers"
    And the response status code should be 201
    And the "name" property should equal "Offre1"

  Scenario: get one offer
    Given I have the payload
    """
    {
          "email": "admin@gmail.com",
          "password": "Password123"
    }
    """
    Given I authenticate with user "<string>" and password "<string>"
    Given I request "GET /offers/@offer_3"
    Then print last response
    And the response status code should be 201
    And the "recruiter" property should equal "@user_2"


