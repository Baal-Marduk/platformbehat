Feature: _User_
  Background:
    Given the following fixtures files are loaded:
      | user     |
      | offer    |

  Scenario:  User
    Given I have the payload
    """
    {
          "email": "admin@gmail.com",
          "password": "Password123"
    }
    """
    Given I authenticate with user "<string>" and password "<string>"
    Then print last response
    Given I have the payload
    """
     {
        "email": "test@gmail.com",
        "roles": [
          "ROLE_ADMIN"
        ],
        "password": "Password123",
        "isActive": true,
        "offer": [],
        "application": []
    }
    """
    Given I request "POST /users"
    When the response status code should be 201
    Then print last response

    Given I have the payload
    """
     {
        "email": "enzo@gmail.com",
        "roles": ['ROLE_RECRUITER'],
        "password": "test",
        "isActive": true,
        "token": "test token"
        "offers": [],
        "applications": []
    }
    """

  Scenario: Get my User
    Given I have the payload
    """
    {
          "email": "admin@gmail.com",
          "password": "Password123"
    }
    """
    Given I authenticate with user "<string>" and password "<string>"
    Given I request "GET /users/@user_2"
    And the response status code should be 201
    And the "email" property should equal "recruiter@gmail.com"
    Then print last response

<<<<<<< HEAD
  Scenario:  Bad Password
    Given I have the payload
    """
    {
          "email": "admin@gmail.com",
          "password": "test"
    }
    """
    Given I request "POST /authentication_token"
    And the response status code should be 401
    Then print last response

=======
>>>>>>> b0047dbb3030141b59eda58de24463d71596ec5b






