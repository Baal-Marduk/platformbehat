<?php
namespace App\Tests\Behat\Manager;
class ReferenceManager
{
    private $references = [];
    public function add($id,$ref): void
    {
        $this->references[$id] = $ref;
    }
    public function get($id)
    {
        return $this->references[$id] ;
    }
}
