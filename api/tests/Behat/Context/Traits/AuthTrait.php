<?php

namespace App\Tests\Behat\Context\Traits;

use App\Entity\User;
use Behat\Behat\Hook\Scope\BeforeScenarioScope;


trait AuthTrait
{
    /**
     * The token to use with HTTP authentication
     *
     * @var string
     */
    protected $token;

    /**
     * @Given /^I authenticate with user "([^"]*)" and password "([^"]*)"$/
     * @param $email
     * @param $password
     */
    public function iAuthenticateWithEmailAndPassword($email, $password)
    {
        $this->iRequest('POST' ,'/authentication_token');
        $this->token = $this->arrayGet($this->getScopePayload(), "token", true);
    }
}
